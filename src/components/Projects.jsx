import React, { Component } from 'react';

import { Tabs, Tab,Grid,Cell, Card,CardTitle, CardText,CardActions,Button,CardMenu,IconButton} from "react-mdl";

class Projects extends Component {
    constructor(props) {
        super(props)
        this.state = { activeTab: 0 };
    }


    toggleCategories() {
        if (this.state.activeTab===0) {
            return (
              <div className="projects-grid">
                <Card
                  shadow={5}
                  style={{ minWidth: "450", margin: "auto" }}
                >
                  <CardTitle
                    style={{
                      color: "White",
                      height: "176",
                      background:
                        "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                    }}
                  >
                    Project resturant Management
                  </CardTitle>
                  <CardText>
                    Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Laborum excepturi veritatis
                    aliquid fuga, dolor eaque id ratione nobis.
                    Placeat possimus, qui voluptas necessitatibus
                    eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                  <CardActions border>
                    <Button colored>Github</Button>
                    <Button colored>Codepen</Button>
                    <Button colored>LiveDemo</Button>
                  </CardActions>
                  <CardMenu style={{ color: "#fff" }}>
                    <IconButton
                      style={{ color: "black" }}
                      name="share"
                    />
                  </CardMenu>
                </Card>

                <Card
                  shadow={5}
                  style={{ minWidth: "450", margin: "auto" }}
                >
                  <CardTitle
                    style={{
                      color: "White",
                      height: "176",
                      background:
                        "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                    }}
                  >
                    My Project 1
                  </CardTitle>
                  <CardText>
                    Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Laborum excepturi veritatis
                    aliquid fuga, dolor eaque id ratione nobis.
                    Placeat possimus, qui voluptas necessitatibus
                    eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                  <CardActions border>
                    <Button colored>Github</Button>
                    <Button colored>Codepen</Button>
                    <Button colored>LiveDemo</Button>
                  </CardActions>
                  <CardMenu style={{ color: "#fff" }}>
                    <IconButton
                      style={{ color: "black" }}
                      name="share"
                    />
                  </CardMenu>
                </Card>

                <Card
                  shadow={5}
                  style={{ minWidth: "450", margin: "auto" }}
                >
                  <CardTitle
                    style={{
                      color: "White",
                      height: "176",
                      background:
                        "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                    }}
                  >
                    My Project 1
                  </CardTitle>
                  <CardText>
                    Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Laborum excepturi veritatis
                    aliquid fuga, dolor eaque id ratione nobis.
                    Placeat possimus, qui voluptas necessitatibus
                    eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                  <CardActions border>
                    <Button colored>Github</Button>
                    <Button colored>Codepen</Button>
                    <Button colored>LiveDemo</Button>
                  </CardActions>
                  <CardMenu style={{ color: "#fff" }}>
                    <IconButton
                      style={{ color: "black" }}
                      name="share"
                    />
                  </CardMenu>
                </Card>

                <Card
                  shadow={5}
                  style={{ minWidth: "450", margin: "auto" }}
                >
                  <CardTitle
                    style={{
                      color: "White",
                      height: "176",
                      background:
                        "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                    }}
                  >
                    My Project 1
                  </CardTitle>
                  <CardText>
                    Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Laborum excepturi veritatis
                    aliquid fuga, dolor eaque id ratione nobis.
                    Placeat possimus, qui voluptas necessitatibus
                    eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                  <CardActions border>
                    <Button colored>Github</Button>
                    <Button colored>Codepen</Button>
                    <Button colored>LiveDemo</Button>
                  </CardActions>
                  <CardMenu style={{ color: "#fff" }}>
                    <IconButton
                      style={{ color: "black" }}
                      name="share"
                    />
                  </CardMenu>
                </Card>
              </div>
            );
        }
        else if (this.state.activeTab===1) {
            return (
                <div className="projects-grid">
                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>


                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>

                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>


                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>
                </div>
            );
        }
        else if (this.state.activeTab===2) {
            return (
                <div className="projects-grid">
                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>


                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>

                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>


                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>
                </div>
            )
        }
        else if (this.state.activeTab===3) {
            return (
                <div className="projects-grid">
                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            Project resturant Management
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>


                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>

                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>


                    <Card
                        shadow={5}
                        style={{ minWidth: "450", margin: "auto" }}
                    >
                        <CardTitle
                            style={{
                                color: "White",
                                height: "176",
                                background:
                                    "url(https://images.pexels.com/photos/953214/pexels-photo-953214.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
                            }}
                        >
                            My Project 1
                  </CardTitle>
                        <CardText>
                            Lorem ipsum dolor sit amet consectetur
                            adipisicing elit. Laborum excepturi veritatis
                            aliquid fuga, dolor eaque id ratione nobis.
                            Placeat possimus, qui voluptas necessitatibus
                            eum et repellat incidunt! Rerum, ea aliquid!
                  </CardText>
                        <CardActions border>
                            <Button colored>Github</Button>
                            <Button colored>Codepen</Button>
                            <Button colored>LiveDemo</Button>
                        </CardActions>
                        <CardMenu style={{ color: "#fff" }}>
                            <IconButton
                                style={{ color: "black" }}
                                name="share"
                            />
                        </CardMenu>
                    </Card>
                </div>
            )
        }
    }

    render() {
        return (
            <div className="category-tabs">
                <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
                    <Tab>JAVA</Tab>
                    <Tab>PHP</Tab>
                    <Tab>JAVASCRIPT</Tab>
                    <Tab>REACTJS</Tab>
                </Tabs>
                
                    <Grid>
                        <Cell col={12}>
                            <div className="content"> {this.toggleCategories()}</div>
                        </Cell>
                    </Grid>
            </div>    
        );
    }
}

export default Projects;