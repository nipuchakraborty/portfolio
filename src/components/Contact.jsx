import React, { Component } from "react";
import { Grid, Cell, List, ListItem, ListItemContent } from "react-mdl";

class Contact extends Component {
  state = {};
  render() {
    return (
      <div className="contact-body">
        <Grid className={"contact-grid"}>
          <Cell col={6}>
            <h2>Nipu Chakraborty</h2>
            <img
              src="https://avatars2.githubusercontent.com/u/26400522?s=400&u=1cf6159fd51685c22aae34ced4667bf07e4f4234&v=4"
              alt="Profile Pic"
            />
            <p>
            Honest -Creative -Proactive these 3 words I describe myself. I am a web developer.
            My skills include PHP,Reactjs,JQuery,Javascript,Laravel,Lumen,Api based Application, Java ,c, c++ ,MySQL, HTML, CSS, Bootstrap and graphics
            design. I also have experience with well graphics design and SEO knowledge.
            Currently, I am learning React js, Vue Js and React native. I love coding & I want to
            make things that make a difference
            </p>
          </Cell>
          <Cell col={6}>
            <h2>Contact Me</h2>
            <hr />
            <div className="contact-list">
              <List>
                <ListItem>
                  <ListItemContent
                    style={{ fontSize: "25px", fontFamily: "Aton" }}
                  >
                    <i className={"fa fa-phone"} aria-hidden="true" />
                    (+088) 01827236486
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent
                    style={{ fontSize: "25px", fontFamily: "Aton" }}
                  >
                    <i className={"fa fa-at"} aria-hidden="true" />
                    pro.nipu@gmail.com
                  </ListItemContent>
                </ListItem>
                <ListItem>
                  <ListItemContent
                    style={{ fontSize: "25px", fontFamily: "Aton" }}
                  >
                    <i className={"fab fa-github"} aria-hidden="true" />
                    Nondukishor
                  </ListItemContent>
                            </ListItem>
                            


                            <ListItem>
                                <ListItemContent
                                    style={{ fontSize: "25px", fontFamily: "Aton" }}
                                >
                                    <i className={"fab fa-skype"} aria-hidden="true" />
                                    Nipuchakraborty
                  </ListItemContent>
                            </ListItem>
              </List>
            </div>
          </Cell>
        </Grid>
      </div>
    );
  }
}

export default Contact;
