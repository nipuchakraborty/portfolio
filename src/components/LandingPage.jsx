import React, { Component } from 'react';
import { Grid, Cell } from "react-mdl";


class LandingPage extends Component {
    render() { 
        return (
          <div style={{ width: "100%", margin: "auto" }}>
            <Grid className="landing-grid">
              <Cell col={12}>
                <img
                  src='https://avatars2.githubusercontent.com/u/26400522?s=400&u=1cf6159fd51685c22aae34ced4667bf07e4f4234&v=4'
                  alt="not loaded"
                  className="avatar-img"
                />
              </Cell>
              <div className="banner-text">
                <h1>FULL STACK WEB DEVELOPER</h1>
                <hr />
                <p>HTML/CSS|BOOTSTRAP|REACTJS|PHP|JAVA|C|C++|PHOTOSHOP|ADOBE ILLASTRATOR</p>
                <div className="social-links align center">
                  <a href="https://www.facebook.com/pro.nipuchakraborty" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-facebook" aria-hidden="true"></i>
                  </a>
                  <a href="https://www.freecodecamp.org/nipu" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-free-code-camp" aria-hidden="true"></i>
                  </a>
                  <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                  <i className="fab fa-twitter-square" aria-hidden='true'></i>
                  </a>
                  <a href="www.google.com" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-github" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </Grid>
          </div>
        );
    }
}
export default LandingPage;