import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import LandingPage from "./LandingPage";
import Aboutme from './Aboutme';
import Projects from './Projects';
import Resume from './Resume';
import Contact from './Contact';
class Main extends Component {
   
    render() { 
        return (
          <Switch>
            <Route exact path={"/"} component={LandingPage} />
            <Route  path={"/about"} component={Aboutme} />
            <Route  path={"/project"} component={Projects} />
            <Route  path={"/resume"} component={Resume} />
            <Route  path={"/contact"} component={Contact} />
          </Switch>
        );
    }
}
 
export default Main;