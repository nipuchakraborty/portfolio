import React, { Component } from 'react';
import { Layout, Header, Navigation, Drawer, Content } from 'react-mdl';
import "./App.css";
import Main from './components/Main';
import { Link } from "react-router-dom";


class App extends Component {
  render() {
    return (
      <div className="App">
        {/* Uses a header that scrolls with the text, rather than staying locked at the top */}
        <div className="demo-big-content">
          <Layout>
            <Header className={"header-color"} title="NIPU CHAKRABORTY">
              <Navigation>
                <Link  className="list-items" to="/resume">Resume</Link>
                <Link  className="list-items" to="/about">About me</Link>
                <Link  className="list-items" to="/project">Projects</Link>
                <Link  className="list-items" to="/contact">Contact</Link>
              </Navigation>
            </Header>
            <Drawer title="NIPU" className={"side-bar"}>
              <Navigation>
                <Link className="list-items" to="/resume">Resume</Link>
                <Link className="list-items" to="/about">About me</Link>
                <Link className="list-items" to="/project">Projects</Link>
                <Link className="list-items" to="/contact">Contact</Link>
              </Navigation>
            </Drawer>
            <Content>
              <div className="page-content" />
              <Main />
            </Content>
          </Layout>
        </div>
      </div>
    );
  }
}

export default App;
